#!/bin/bash

selected=$(echo "Run\nSuspend\nShutdown" | rofi -dmenu -p Menu -location 1 -width 400 -theme-str "@theme \"~/.config/rofi/theme/menutheme.rasi\"" -font "Hasklug NF Medium 10")
echo $selected

case $selected in
	Run)
		rofi -show run -p Run -location 1 -width 400 -theme-str "@theme \"~/.config/rofi/theme/menutheme.rasi\"" -font "Hasklug NF Medium 10";;
	Suspend)
		ans=$(echo "No\nYes" | rofi -dmenu -p "Suspend ?" -location 1 -width 400 -theme-str "@theme \"~/.config/rofi/theme/menutheme.rasi\"" -font "Hasklug NF Medium 10")
		case $ans in
			Yes)
				systemctl suspend;;
		esac
		;;
	Shutdown)
		ans=$(echo "No\nYes" | rofi -dmenu -p "Shutdown ?" -location 1 -width 400 -theme-str "@theme \"~/.config/rofi/theme/menutheme.rasi\"" -font "Hasklug NF Medium 10")
		case $ans in
			Yes)
				shutdown -h now;;
		esac
		;;
esac
